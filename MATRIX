#!/usr/bin/env bash

set -x #verbose output
set -e #fail in case of any error

. ./common-funcs.sh || exit 1

fail(){
	echo "Critical failure, terminating."
	exit 1
}

umount_chroot_bind_nofail(){
	for umount_point in proc 'dev/pts' dev sys
	do
		umount -l "${ch}/${umount_point}" || :
	done
}

RESULTS_DIR="${RESULTS_DIR:-/home/vagrant/results}"
# /home/vagrant/results is read by ABF to take build results,
# but let's try using local directory not on ABF.
# Change RESULTS_DIR if it was not redefined.
# /home/vagrant/results preexists on ABF.
[ ! -d '/home/vagrant/results' ] && \
	[ "$RESULTS_DIR" = '/home/vagrant/results' ] && \
	RESULTS_DIR="${PWD}/results"
PRODUCTNAME="ROSA.DESKTOP.$RELEASE"
ch="${ch:-${PRODUCTNAME}}"
ROSA_PLATFORM="${ROSA_PLATFORM:-rosa2016.1}"
SLEEP="${SLEEP:-0}"

# This is bad, but for now building ISOs of multiple archs
# at one time is broken, so take the first arch in $TYPES.
type="$(echo "$TYPES" | awk '{print $1}')"
set_types
ARCH="$arch"

MIRROR="${MIRROR:-http://abf-downloads.rosalinux.ru}"
REPO="${REPO:-${MIRROR}/${ROSA_PLATFORM}/repository/${ARCH}/}"

if [ "$SLEEP" -gt 0 ]; then sleep "$SLEEP"; fi

# Install required packages
# List of packages was generated automatically by find_requires_ALT in common-funcs.sh
# lsof is from .ks.template, I added it to this list manually
# Install packages one-by-one, not urpmi <list>, to prevent marking them as manually installed
for i in coreutils findutils sed tar urpmi util-linux lsof
do
	rpm -q "$i" >/dev/null 2>/dev/null  || urpmi --auto "$i"
done

umount_chroot_bind_nofail

for dir in dev '/dev/pts' proc sys "opt/ISOBUILD"
do
	[ ! -d "${ch}/${dir}" ] && mkdir -p "${ch}/${dir}"
done
[ ! -d "${RESULTS_DIR}" ] && mkdir -p "${RESULTS_DIR}"

# copy current git to chroot to execute build scripts in chroot
tar -c --exclude "${ch}" . | tar -x -C "${ch}/opt/ISOBUILD/"
ls -R > "${RESULTS_DIR}/ls.log"

urpmi.addmedia --distrib --mirrorlist "$REPO" --urpmi-root "${ch}"

#########################################################
# try to workaround urpmi bug due to which it randomly
# can't resolve dependencies during bootstrap
urpmi_bootstrap(){
	for urpmi_options in \
		"--auto --no-suggests --allow-force --allow-nodeps --ignore-missing" \
		"--auto --no-suggests"
	do
		urpmi --urpmi-root "${ch}" --root "${ch}" \
			${urpmi_options} \
			basesystem-minimal \
			basesystem \
			urpmi rpm \
			locales-en locales-ru \
			rpm-build livecd-tools syslinux \
			bind-utils \
			parallel bc
		urpmi_return_code="$?"
	done
}
# temporarily don't fail the whole scripts when not last iteration of urpmi fails
set +e
for i in $(seq 1 5)
do
	urpmi_bootstrap
	if [ "${urpmi_return_code}" = 0 ]; then break; fi
done
# now check the return code of the _last_ urpmi iteration
if [ "${urpmi_return_code}" != 0 ]; then
	echo "urpmi bootstrapping failed!"
	exit 1
fi
# return failing the whole script on any error
set -e
#########################################################

for mount_point in proc 'dev/pts' dev sys
do
	mount --bind "/${mount_point}" "${ch}/${mount_point}"
done
cp /etc/resolv.conf "${ch}/etc/"

echo
echo "----------> UR IN Z MATRIX <----------"

chroot "${ch}" /bin/sh -c "cd /opt/ISOBUILD && env BUILD_ID=${BUILD_ID} /opt/ISOBUILD/build"
find "${ch}/opt/" -type f -name 'ROSA*.iso' -exec cp -v {} "${RESULTS_DIR}/"  \;

dir0="$PWD"
cd "${RESULTS_DIR}"
	( set +f
	for i in *.iso
	do
		md5sum "$i" > "${i}.md5sum"
	done )
cd "$dir0"
